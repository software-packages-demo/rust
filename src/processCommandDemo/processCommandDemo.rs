use std::process::Command;

fn main() {
    println!("1. spawn");

    let mut child1 = Command::new("sh")
        .arg("-c").arg("echo 2. hello")
        .spawn()
        .expect("failed to execute process");

    println!("3. Now in Rust");

    let mut child2 = Command::new("sh")
        .arg("-c").arg("echo 4. bye...")
        .spawn()
        .expect("failed to execute process");

    assert!(child2.wait()
        .expect("failed to wait on child")
        .success());
    assert!(child1.wait()
        .expect("failed to wait on child")
        .success());

    println!("5. status");

    assert!(Command::new("sh")
        .arg("-c").arg("echo 6. hello")
        .status()
        .expect("failed to execute process")
        .success());

    println!("7. Now in Rust");

    assert!(Command::new("sh")
        .arg("-c").arg("echo 8. bye...")
        .status()
        .expect("failed to execute process")
        .success());
}
