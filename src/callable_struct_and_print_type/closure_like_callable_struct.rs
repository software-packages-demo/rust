#[derive(Debug)]
pub struct OnceExample {
    pub s : String
}

impl FnOnce<()> for OnceExample {
    type Output = ();
    extern "rust-call" fn call_once(self, _args: ()) {
        println!("{}", self.s);
    }
    // #![feature(fn_traits)]
    // #![feature(unboxed_closures)]
    // error[E0658]: the precise format of `Fn`-family traits' type parameters is subject to change
    // error[E0658]: rust-call ABI is subject to change
    // note: for more information, see https://github.com/rust-lang/rust/issues/29625
}

pub struct I128Arg {
    pub s : String
}

impl FnOnce<(i128,)> for I128Arg {
    type Output = ();
    extern "rust-call" fn call_once(self, args: (i128,)) {
        println!("{}{}", self.s, args.0);
    }
}

#[derive(Debug)]
pub struct MutExample {
    pub s : String
}

impl FnOnce<()> for MutExample {
    type Output = ();
    extern "rust-call" fn call_once(self, _args: ()) {
        println!("{}", self.s);
    }
}

// impl<A, F: Fn<A> + ?Sized> Fn<A> for Box<F> // other possibilities...
impl FnMut<()> for MutExample
{
    // type Output = ();
    extern "rust-call" fn call_mut(&mut self, _args: ())
    // extern "rust-call" fn call_once(self, _args: ())
    {
        println!("{}", &(self.s));
    }
    // extern "rust-call" fn call_mut(&mut self, _args: ())
        // -> <MutClosure as FnOnce<()>>::Output
    // {
        // println!("{}", &(self.s));
    // }
}

#[derive(Debug)]
pub struct ImmutExample {
    pub s : String
}

impl FnOnce<()> for ImmutExample {
    type Output = ();
    extern "rust-call" fn call_once(self, _args: ()) {
        println!("{}", self.s);
    }
}

impl FnMut<()> for ImmutExample
{
    extern "rust-call" fn call_mut(&mut self, _args: ())
    {
        println!("{}", &(self.s));
    }
}

impl Fn<()> for ImmutExample
{
    extern "rust-call" fn call(&self, _args: ())
    {
        println!("{}", &(self.s));
    }
}
