mod print_type_of;

fn main() {
    let param = 3.14;
    let closure = ||{println!("closure with: {}", param);};
    print_type_of::print_type_of(&param);
    print_type_of::print_type_of(&closure);
}