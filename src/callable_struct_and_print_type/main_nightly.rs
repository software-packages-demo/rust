#![feature(core_intrinsics)]
#![feature(fn_traits)]
#![feature(unboxed_closures)]

mod closure_like_callable_struct;
use closure_like_callable_struct::{
    OnceExample,
    I128Arg,
    MutExample,
    ImmutExample
};

mod print_type_of;
use print_type_of::print_type_of;

fn main() {
    let ocfs = OnceExample{s: String::from("once closure from struct")};
    print_type_of(&ocfs);
    println!("{:?}", &ocfs);
    ocfs();
    // ocfs(); // FnOnce error[E0382]: use of moved value: `cfs`

    OnceExample{s: String::from("another closure from struct")}();

    I128Arg{s: String::from("thirty-nine: ")}(39);

    let mut mcfs = MutExample{s: String::from("mut closure from struct")};
    print_type_of(&mcfs);
    println!("{:?}", &mcfs);
    mcfs();
    mcfs();

    let icfs = ImmutExample{s: String::from("immut closure from struct")};
    print_type_of(&icfs);
    println!("{:?}", &icfs);
    icfs();
    icfs();
}

// The Unstable Book
// https://doc.rust-lang.org/unstable-book/

// What is a crate attribute and where do I add it?
// https://stackoverflow.com/questions/27454761/what-is-a-crate-attribute-and-where-do-i-add-it

// How do I print the type of a variable in Rust?
// https://stackoverflow.com/questions/21747136/how-do-i-print-the-type-of-a-variable-in-rust

