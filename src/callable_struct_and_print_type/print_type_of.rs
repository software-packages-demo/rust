pub fn print_type_of<T>(_: &T) {
    println!("{}", std::intrinsics::type_name::<T>());
}
// error[E0658]: use of unstable library feature 'core_intrinsics': intrinsics are unlikely to ever be stabilized, instead they should be used through stabilized interfaces in the rest of the standard library
// help: add `#![feature(core_intrinsics)]` to the crate attributes to enable

