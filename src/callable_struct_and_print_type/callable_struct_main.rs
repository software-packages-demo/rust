mod closure_like_callable_struct;
use closure_like_callable_struct::{
    OnceExample,
    I128Arg,
    MutExample,
    ImmutExample
};

fn main() {
    let ocfs = OnceExample{s: String::from("once closure from struct")};
    println!("{:?}", &ocfs);
    ocfs();
    // ocfs(); // FnOnce error[E0382]: use of moved value: `cfs`

    OnceExample{s: String::from("another closure from struct")}();

    I128Arg{s: String::from("thirty-nine: ")}(39);

    let mut mcfs = MutExample{s: String::from("mut closure from struct")};
    println!("{:?}", &mcfs);
    mcfs();
    mcfs();

    let icfs = ImmutExample{s: String::from("immut closure from struct")};
    println!("{:?}", &icfs);
    icfs();
    icfs();
}
