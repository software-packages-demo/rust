#![feature(core_intrinsics)]

mod print_type_of;
use print_type_of::print_type_of;

fn main() {
    let param = 3.14;
    let closure = ||{println!("closure with: {}", param);};
    print_type_of(&param);
    print_type_of(&closure);

   // let vec_new = Vec::new();
    // error[E0282]: type annotations needed for `std::vec::Vec<T>`
    let vec_square = vec![1, 2, 3, 4, 5];
    print_type_of(&vec_square);
    let vec_round = vec!(1, 2, 3, 4, 5);
    print_type_of(&vec_round);
}
